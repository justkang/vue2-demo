import Vue from 'vue'
import VueRouter from "vue-router";
Vue.use(VueRouter)
const routes = [
  {
    path:'/',
    component:()=>import('@/views/home')
  },
  {
    path: '/autoElementTableButton',
    component: () => import('@/views/autoElementTableButton')
  },
  {
    path: '/HelloWorld',
    component: () => import('@/views/HelloWorld.vue')
  },
  {
    path: '/TestFullCalendar',
    component: () => import('@/views/TestFullCalendar.vue')
  },
  {
    path: '/WebSpeechiApi',
    component: () => import('@/views/WebSpeechApi.vue')
  },
  {
    path: 'echartsDemo',
    component: () => import('@/views/echartsDemo')
  }
]
const router = new VueRouter({
  routes
})

export default router

